#!/usr/bin/python3

from __future__ import division
import numpy as np
import pandas as pd 
from pandas import DataFrame,Series
import matplotlib as mlt
import matplotlib.pyplot as plt 
import seaborn as sns
from scipy import stats
from scipy.stats import pearsonr
from numpy.random import random as rdn

# dataset1=rdn(100)
# dataset2=rdn(100)
# sns.set(style='darkgrid')
# visualize=sns.jointplot(dataset1,dataset2,kind='kde').annotate(stats.pearsonr)
# plt.show(visualize)

# import dataset
dataset=pd.read_csv('breast_cancer.csv')

# Dataset summary 
print(dataset.head())
# print(dataset.columns)

# print(dataset.describe())
# print(dataset['class'].describe())
# print(dataset['class'].value_counts())
# print(dataset['age'].isnull().sum())


# Visualizations
# histogram
# histo = dataset['menopause'].hist(bins=200)
# plt.show(sns)

# # kde plot
kd = sns.kdeplot(dataset['degmalig                      '], shade=True)
plt.show(sns)

# TODO category encoding
# from sklearn.preprocessing import LabelEncoder, OneHotEncoder
# labelencoder_X = LabelEncoder()
# X[:, 0] = labelencoder_X.fit_transform(X[:, 0])onehotencoder = OneHotEncoder(categorical_features = [0])
# X = onehotencoder.fit_transform(X).toarray()labelencoder_y = LabelEncoder()
# y = labelencoder_y.fit_transform(y)

# encoding manually
encode_dic = {'monopause': {'premeno':0, 'lt40':1, 'ge40': 2}}
cleaned = dataset.replace(encode_dic, inplace=True)
print(cleaned)







# # split training and testing
# from sklearn.model_selection import train_test_split
# X_train, X_test, Y_train, Y_test = train_test_split( X , Y , test_size = 0.2, random_state = 0)


# # stanadizing Euclidean distance using standard scaler
# from sklearn.preprocessing import StandardScaler
# sc_X = StandardScaler()
# # transform all data to same standardized scale
# X_train = sc_X.fit_transform(X_train)
# X_test = sc_X.transform(X_test)






