# Breast cancer recurrence model

* Data Collection
* Data Preparation
* EDA
* Model
* Deployment
* Optimization


* Data Collection
<!-- source -->


* Data preparation
no missing, incosistence

shape = (276, 11)

columns = ['age', 'menopause', 'tumorsize', 'invnodes','nodecaps', 'degmalig', 'breast', 'breastquad', 'irradiant', 'class', 'id']

* EDA
age
range = 20 to 60 yrs
mode = 50 - 59 
<!-- what of those over 50, why manifest later in life? -->
<!-- visualize distribution -->

menopause
mode = premono = 143 
ge40 = 126
lt40 = 7
<!-- what are the different stages of a menopause -->
<!-- Age seems like a cancer recurrence factor -->
<!-- During deployment, perhaps increase check with check. What are the implications -->

tumor size
range = 0 - 50
mode = 20 - 35
<!-- unit of measurement? whats used to measure? -->
<!-- visualize distribution -->


invnodes
mode = 0 -2
<!-- what are they?  -->
<!-- distribution -->

nodecaps
mode = no = 213 
yes = 55
dont know = nil
<!-- what are they? -->
<!-- those that donot know nodecaps whats with them? -->

degmalig
mean = 2.058
3 = 84
1 = 68
<!-- what is it? -->

Breast
mode = top left = 145

Breastquad
mode = leftlow
left up =96
<!-- cluster distribution -->

class
no recurrence = 196
<!-- After how long -->

irradiant
mode = 210  
<!-- meaning -->


<!-- compare all variables against each other -->
<!-- Is data biased to whites, female -->




